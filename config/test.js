const path = require('path');

module.exports = {
  name: 'Awaz',
  paths: {
    app: {
      client: path.resolve(__dirname, '../src/client'),
      api: path.resolve(__dirname, '../src/server/app'),
      server: path.resolve(__dirname, '../src/server/core')
    },
    dist: {
      server: path.resolve(__dirname, '../dist'),
      client: path.resolve(__dirname, '../dist/client')
    },
    static: path.resolve(__dirname, '../static')
  },
  server: {
    port: 3000,
    compress: false
  },
  db: {
		host: 'localhost',
		dbName: 'awaz',
		debug: false,
		options: {
			userName: false,
			passWord: false,
			port: 27017
		}
	},
	secret: [
    'yoursecretkey'
  ],
  smsApi: {
    url: 'http://mobi1.blogdns.com/httpmsgid/SMSSenders.aspx',
    username: 'Hariinfotemplet',
    password: 'h123',
    gsmid: 'gsmid'
  },
  file: {
    size: '200mb'
  },
  nuxtBuild: false
};
