import { Crud } from '@utl';

import { userModel } from './';
import { filesModel } from '../files';
import { statusCrud } from './status.model';

const { filesCrud } = filesModel;

class UserService extends Crud {
  constructor(model) {
    super(model);
    this.model = model;
    this.filesCrud = filesCrud;
    this.statusCrud = statusCrud;
  }

  async updatePhone(options) {
    const record = await this.single(options.params);
    record.phone = record.temp_phone;
    record.temp_phone = '';
    return new Promise((resolve, reject) => {
      record.save().then((result) => {
        resolve(result);
      }).catch((e) => {
        reject(e);
      });
    });
  }

  async updateStatus(options) {
    const user = await this.single(options.params);
    const record = await this.statusCrud.single({
      qr: {
        _id: user.status
      }
    });
    record.post = options.body.post;
    return new Promise((resolve, reject) => {
      record.save().then((result) => {
        resolve(result);
      }).catch((e) => {
        reject(e);
      });
    });
  }
}

const userCrud = new UserService(userModel);
export default userCrud;
