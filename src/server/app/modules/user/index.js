export * as userRouteProps from './router';
export {
  userSingle,
  userUpdate,
  userDelete,
  userCheck,
  userOtpResend,
  userOtpVerify,
  getUserById,
  userChangeNumber,
  userUpdateStatus,
  socketTest
} from './controller';
export { default as userModel } from './user.model';
export { default as userCrud } from './user.service';
export { default as userIO } from './user.io';
