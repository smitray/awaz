import { userCrud } from './';
import { permissionCrud } from './permission.model';

function userStream(socket, app) {
  socket.on('contact-sync-init', async ({ from, totalContact }) => {
    try {
      const user = await userCrud.single({
        qr: {
          _id: from
        }
      });
      await permissionCrud.put({
        params: {
          qr: {
            _id: user.permission
          }
        },
        body: {
          totalContact
        }
      });
      app.io.to(from).emit('contact-sync-init', {
        totalContact
      });
    } catch (e) {
      throw new Error(e.message);
    }
  });
  socket.on('contact-sync', async ({ phone, from }) => {
    try {
      const friend = await userCrud.single({
        qr: {
          phone
        },
        populate: [{
          path: 'dp',
          model: 'filesModel'
        }, {
          path: 'status',
          model: 'statusModel'
        }, {
          path: 'permission',
          model: 'permissionModel',
          select: 'blocked friends'
        }]
      });
      const user = await userCrud.single({
        qr: {
          _id: from
        },
        populate: [{
          path: 'dp',
          model: 'filesModel'
        }, {
          path: 'status',
          model: 'statusModel'
        }, {
          path: 'permission',
          model: 'permissionModel',
          select: 'blocked friends'
        }]
      });
      const permission = await permissionCrud.single({
        qr: {
          _id: user.permission
        }
      });
      let retObj;
      permission.totalContact -= 1;

      if (friend) {
        const isInArray = permission.friends.some(frnd => frnd.equals(friend._id));
        if (isInArray) {
          retObj = {
            success: 1,
            message: 'contact already exists',
            user: friend,
            remaining: permission.totalContact,
            phone
          };
        } else {
          permission.friends.push(friend._id);
          retObj = {
            user: friend,
            remaining: permission.totalContact,
            success: 1,
            message: 'new contact added',
            phone
          };
          const prem = await permissionCrud.single({
            qr: {
              _id: friend.permission
            }
          });
          prem.totalContact += 1;
          prem.friends.push(user._id);
          app.io.to(friend._id).emit('contact-request', {
            user,
            success: 1,
            message: 'new contact added'
          });
        }
      } else {
        retObj = {
          success: 0,
          message: 'contact is not registered',
          remaining: permission.totalContact,
          phone
        };
      }
      await permission.save();
      app.io.to(from).emit('contact-sync', retObj);
    } catch (e) {
      throw new Error(e.message);
    }
  });

  socket.on('delete-contact', async ({ from }) => {
    try {
      const user = await userCrud.single({
        qr: {
          _id: from
        },
        populate: [{
          path: 'permission',
          model: 'permissionModel'
        }]
      });
      
    } catch (e) {
      throw new Error(e.message);
    }
  });

  socket.on('user-join', ({ uid }) => {
    socket.join(uid);
    app.io.to(uid).emit('conf-message', {
      message: 'User created on socket',
      uid
    });
  });
}

export default userStream;
