import config from 'config';
import request from 'request-promise';
import randomize from 'randomatic';
import { generateJwt } from '@utl';


import { userCrud } from './';
import { permissionCrud } from './permission.model';
import { statusCrud } from './status.model';

let user;
let userNew;
let token;


const sendOtp = (phone) => {
  const otp = randomize('0', 6);
  const reqOpt = {
    method: 'GET',
    uri: config.get('smsApi.url'),
    qs: {
      UserID: config.get('smsApi.username'),
      UserPass: config.get('smsApi.password'),
      Message: `Your ${config.get('name')} OTP is ${otp}`,
      MobileNo: phone,
      GSMID: 'helpln'
    },
    json: true
  };
  request(reqOpt);
  return otp;
};

const userSingle = async (ctx) => {
  try {
    user = await userCrud.single({
      qr: {
        _id: ctx.state.user.uid
      },
      select: 'full_name gender occupation dob email city state country blood_group donor phone dp status permission last_seen',
      populate: [{
        path: 'dp',
        model: 'filesModel'
      }, {
        path: 'status',
        model: 'statusModel'
      }, {
        path: 'permission',
        model: 'permissionModel'
      }]
    });
  } catch (e) {
    ctx.throw(404, {
      success: 0,
      message: e.message
    });
  } finally {
    ctx.body = {
      success: 1,
      data: {
        user
      },
      message: 'User found'
    };
  }
};

const getUserById = async (ctx) => {
  try {
    user = await userCrud.single({
      qr: {
        _id: ctx.params.id
      },
      select: 'full_name gender occupation dob email city state country blood_group donor phone dp status last_seen',
      populate: [{
        path: 'dp',
        model: 'filesModel'
      }, {
        path: 'status',
        model: 'statusModel'
      }]
    });
  } catch (e) {
    ctx.throw(404, {
      success: 0,
      message: e.message
    });
  } finally {
    ctx.body = {
      success: 1,
      data: {
        user
      },
      message: 'User found'
    };
  }
};

const userUpdate = async (ctx) => {
  try {
    user = await userCrud.put({
      params: {
        qr: {
          _id: ctx.state.user.uid
        }
      },
      body: ctx.request.body
    });
  } catch (e) {
    ctx.throw(422, {
      success: 0,
      message: e.message
    });
  } finally {
    ctx.body = {
      success: 1,
      data: {
        user
      },
      message: 'User details updated'
    };
  }
};


const userDelete = async (ctx) => {
  try {
    user = await userCrud.delete({
      params: {
        qr: {
          _id: ctx.state.user.uid
        }
      }
    });
  } catch (e) {
    ctx.throw(404, {
      success: 0,
      message: e.message
    });
  } finally {
    ctx.body = {
      success: 1,
      message: 'User deleted'
    };
  }
};

const userUpdateStatus = async (ctx) => {
  let status;
  try {
    status = await userCrud.updateStatus({
      params: {
        qr: {
          _id: ctx.state.user.uid
        }
      },
      body: ctx.request.body
    });
  } catch (e) {
    ctx.throw(422, {
      success: 0,
      message: e.message
    });
  } finally {
    ctx.body = {
      success: 1,
      data: {
        status
      },
      message: 'User status updated'
    };
  }
};


const userCheck = async (ctx) => {
  const { phone } = ctx.request.body;
  user = await userCrud.single({
    qr: {
      phone
    },
    select: 'user_name dp status permission last_seen'
  });
  const otp = sendOtp(phone);
  if (user) {
    try {
      user = await userCrud.put({
        params: {
          qr: {
            phone
          },
          select: 'user_name dp status permission last_seen'
        },
        body: {
          otp
        }
      });
    } catch (e) {
      ctx.throw(422, {
        success: 0,
        message: e.message
      });
    } finally {
      token = await generateJwt({
        uid: user._id
      });
      ctx.body = {
        success: 1,
        data: {
          userType: 'exist',
          token
        },
        message: 'Existing user found'
      };
    }
  } else {
    try {
      const permission = await permissionCrud.create({});
      const status = await statusCrud.create({});
      userNew = await userCrud.create({
        phone,
        otp,
        permission: permission._id,
        status: status._id
      });
    } catch (e) {
      ctx.throw(422, {
        success: 0,
        message: e.message
      });
    } finally {
      token = await generateJwt({
        uid: userNew._id
      });
      ctx.body = {
        success: 1,
        data: {
          userType: 'new',
          token
        },
        message: 'New user created'
      };
    }
  }
};


const userOtpResend = async (ctx) => {
  const { phone } = ctx.request.body;
  const otp = sendOtp(phone);
  try {
    user = await userCrud.put({
      params: {
        qr: {
          phone
        }
      },
      body: {
        otp
      }
    });
  } catch (e) {
    ctx.throw(422, {
      success: 0,
      message: e.message
    });
  } finally {
    ctx.body = {
      success: 1,
      message: 'New OTP sent to registered mobile number'
    };
  }
};


const userOtpVerify = async (ctx) => {
  const { otp } = ctx.request.body;
  try {
    user = await userCrud.single({
      qr: {
        _id: ctx.state.user.uid,
        otp
      },
      select: 'user_name dp status permission last_seen'
    });
  } catch (e) {
    ctx.throw(404, {
      success: 0,
      message: e.message
    });
  } finally {
    let data = {
      success: 0,
      message: 'User not found'
    };

    if (user !== null) {
      data = {
        success: 1,
        data: {
          user
        },
        message: 'User verified'
      };
    }
    ctx.body = data;
  }
};

const userChangeNumber = async (ctx) => {
  const { otp, phone } = ctx.request.body;
  if (otp) {
    try {
      user = await userCrud.updatePhone({
        params: {
          qr: {
            temp_phone: phone,
            otp
          }
        }
      });
    } catch (e) {
      ctx.throw(404, {
        success: 0,
        message: e.message
      });
    } finally {
      ctx.body = {
        success: 1,
        message: 'Phone number changed'
      };
    }
  } else {
    const otpgen = sendOtp(phone);
    try {
      user = await userCrud.put({
        params: {
          qr: {
            _id: ctx.state.user.uid
          }
        },
        body: {
          temp_phone: phone,
          otp: otpgen
        }
      });
    } catch (e) {
      ctx.throw(404, {
        success: 0,
        message: e.message
      });
    } finally {
      ctx.body = {
        success: 1,
        message: 'OTP sent to updated mobile number'
      };
    }
  }
};

const contactSync = async (ctx) => {
  const {
    phone
  } = ctx.request.body;
  const totalContact = phone.length;
  const users = [];
  try {
    await Promise.all(phone.map(async (ph) => {
      const friend = await userCrud.single({
        qr: {
          phone: ph
        },
        populate: [{
          path: 'dp',
          model: 'filesModel'
        }, {
          path: 'status',
          model: 'statusModel'
        }]
      });
      user = await userCrud.single({
        qr: {
          _id: ctx.state.user.uid
        }
      });
      if (friend) {
        const permission = await permissionCrud.single({
          qr: {
            _id: user.permission,
            friends: friend._id
          }
        });
        if (!permission) {
          const perm = await permissionCrud.single({
            qr: {
              _id: user.permission
            }
          });
          perm.friends.push(friend._id);
          perm.save();
        }
        users.push({
          success: 1,
          friend
        });
      } else {
        users.push({
          success: 0,
          phone: ph
        });
      }
    }));
    ctx.body = {
      users,
      totalContact
    };
  } catch (e) {
    ctx.throw(404, {
      success: 0,
      message: e.message
    });
  }
};

const socketTest = (ctx) => {
  ctx.app.io.to('123').emit('message', 'this is a test from controller');
  ctx.body = {
    success: 1,
    message: 'OTP sent to updated mobile number'
  };
};

export {
  userSingle,
  userUpdate,
  userDelete,
  userCheck,
  userOtpResend,
  userOtpVerify,
  getUserById,
  userChangeNumber,
  userUpdateStatus,
  contactSync,
  socketTest
};
