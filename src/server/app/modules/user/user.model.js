import mongoose from 'mongoose';
import uniqueValidator from 'mongoose-unique-validator';
import timestamp from 'mongoose-timestamp';


const userSchema = new mongoose.Schema({
  full_name: {
    type: String,
    default: null
  },
  gender: {
    type: String,
    default: null
  },
  occupation: {
    type: String,
    default: null
  },
  dob: {
    type: Date,
    default: Date.now()
  },
  email: {
    type: String,
    default: null
  },
  city: {
    type: String,
    default: null
  },
  state: {
    type: String,
    default: null
  },
  country: {
    type: String,
    default: 'India'
  },
  blood_group: {
    type: String,
    default: null
  },
  donor: {
    type: Boolean,
    default: false
  },
  phone: {
    type: String,
    required: true,
    unique: true
  },
  temp_phone: {
    type: String,
    default: null
  },
  otp: {
    type: String
  },
  dp: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'filesModel',
    default: null
  },
  status: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'statusModel',
    default: null
  },
  permission: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'permissionModel',
    default: null
  },
  last_seen: {
    type: Date,
    default: Date.now()
  }
});

userSchema.plugin(uniqueValidator);
userSchema.plugin(timestamp);

const userModel = mongoose.model('userModel', userSchema);

export default userModel;
