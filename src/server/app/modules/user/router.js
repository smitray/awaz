import { isAuthenticated } from '@mid';

import {
  userSingle,
  userUpdate,
  userDelete,
  userCheck,
  userOtpResend,
  userOtpVerify,
  getUserById,
  userChangeNumber,
  userUpdateStatus,
  contactSync,
  socketTest
} from './controller';

export const baseUrl = '/api/user';

export const routes = [
  {
    method: 'GET',
    route: '/',
    handlers: [
      isAuthenticated,
      userSingle
    ]
  },
  {
    method: 'GET',
    route: '/:id',
    handlers: [
      isAuthenticated,
      getUserById
    ]
  },
  {
    method: 'PUT',
    route: '/',
    handlers: [
      isAuthenticated,
      userUpdate
    ]
  },
  {
    method: 'DELETE',
    route: '/',
    handlers: [
      isAuthenticated,
      userDelete
    ]
  },
  {
    method: 'POST',
    route: '/status',
    handlers: [
      isAuthenticated,
      userUpdateStatus
    ]
  },
  {
    method: 'POST',
    route: '/check',
    handlers: [
      userCheck
    ]
  },
  {
    method: 'POST',
    route: '/resend',
    handlers: [
      userOtpResend
    ]
  },
  {
    method: 'POST',
    route: '/verify',
    handlers: [
      isAuthenticated,
      userOtpVerify
    ]
  },
  {
    method: 'POST',
    route: '/change',
    handlers: [
      isAuthenticated,
      userChangeNumber
    ]
  },
  {
    method: 'POST',
    route: '/contact',
    handlers: [
      isAuthenticated,
      contactSync
    ]
  },
  {
    method: 'POST',
    route: '/socket',
    handlers: [
      socketTest
    ]
  }
];
