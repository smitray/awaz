import mongoose from 'mongoose';
import uniqueValidator from 'mongoose-unique-validator';
import timestamp from 'mongoose-timestamp';
import { Crud } from '@utl';

const statusSchema = new mongoose.Schema({
  post: {
    type: String,
    default: 'Suno Sunao, Dil Se'
  },
  wall: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'filesModel',
    default: null
  }]
});

statusSchema.plugin(uniqueValidator);
statusSchema.plugin(timestamp);

const statusModel = mongoose.model('statusModel', statusSchema);
const statusCrud = new Crud(statusModel);

export {
  statusCrud,
  statusModel
};
