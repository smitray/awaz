import mongoose from 'mongoose';
import uniqueValidator from 'mongoose-unique-validator';
import timestamp from 'mongoose-timestamp';
import { Crud } from '@utl';

const permissionSchema = new mongoose.Schema({
  blocked: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'userModel',
    default: null
  }],
  friends: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'userModel',
    default: null
  }],
  totalContact: {
    type: Number
  }
});

permissionSchema.plugin(uniqueValidator);
permissionSchema.plugin(timestamp);

const permissionModel = mongoose.model('permissionModel', permissionSchema);
const permissionCrud = new Crud(permissionModel);

export {
  permissionCrud,
  permissionModel
};
