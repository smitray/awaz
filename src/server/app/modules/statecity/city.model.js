import mongoose from 'mongoose';
import uniqueValidator from 'mongoose-unique-validator';
import timestamp from 'mongoose-timestamp';
import { Crud } from '@utl';

const citySchema = new mongoose.Schema({
  cityName: String,
  stateId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'stateModel',
    default: null
  }
});

citySchema.plugin(uniqueValidator);
citySchema.plugin(timestamp);

const cityModel = mongoose.model('cityModel', citySchema);
const cityCrud = new Crud(cityModel);

export {
  cityModel,
  cityCrud
};
