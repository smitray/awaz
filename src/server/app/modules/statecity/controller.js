import { stateCrud } from './state.model';
import { cityCrud } from './city.model';
import { countryCrud } from './country.model';


let state;
let city;
let country;

/**
@api {post} /api/country Create country
@apiName Create country
@apiGroup Country
@apiParam {String} countryName Name of the country
@apiParamExample {json} Input
{
  countryName: "India"
}
@apiSuccessExample {json} Success
{
  success: 1,
  data: {
    country: [Object]
  },
  message: country created successfully
}
@apiErrorExample {json} Server error
  HTTP/1.1 500 Internal Server Error
*/

export const countryCreate = async (ctx) => {
  try {
    country = await countryCrud.create(ctx.request.body);
    ctx.body = {
      success: 1,
      data: {
        country
      },
      message: 'country created successfully'
    };
  } catch (e) {
    ctx.throw(422, {
      success: 0,
      message: e.message
    });
  }
};

/**
@api {post} /api/state Create state
@apiName Create state
@apiGroup State
@apiParam {String} stateName Name of the state
@apiParam {ObjectId} countryId ID of the country
@apiParamExample {json} Input
{
  stateName: "Karnataka",
  countryId: [ObjectId]
}
@apiSuccessExample {json} Success
{
  success: 1,
  data: {
    state: [Object]
  },
  message: state created successfully
}
@apiErrorExample {json} Server error
  HTTP/1.1 500 Internal Server Error
*/

export const stateCreate = async (ctx) => {
  try {
    state = await stateCrud.create(ctx.request.body);
    ctx.body = {
      success: 1,
      data: {
        state
      },
      message: 'state created successfully'
    };
  } catch (e) {
    ctx.throw(422, {
      success: 0,
      message: e.message
    });
  }
};

/**
@api {post} /api/city Create city
@apiName Create city
@apiGroup City
@apiParam {String} cityName Name of the city
@apiParam {ObjectId} stateId ID of the state
@apiParamExample {json} Input
{
  cityName: "Bangalore",
  stateId: [ObjectId]
}
@apiSuccessExample {json} Success
{
  success: 1,
  data: {
    city: [Object]
  },
  message: city created successfully
}
@apiErrorExample {json} Server error
  HTTP/1.1 500 Internal Server Error
*/

export const cityCreate = async (ctx) => {
  try {
    city = await cityCrud.create(ctx.request.body);
    ctx.body = {
      success: 1,
      data: {
        city
      },
      message: 'city created successfully'
    };
  } catch (e) {
    ctx.throw(422, {
      success: 0,
      message: e.message
    });
  }
};

/**
@api {get} /api/country Get all countries
@apiName Get all countries
@apiGroup Country
@apiSuccessExample {json} Success
{
  success: 1,
  data: {
    country: [Object]
  },
  message: country fetched successfully
}
@apiErrorExample {json} Server error
  HTTP/1.1 500 Internal Server Error
 */

export const countryGet = async (ctx) => {
  try {
    country = await countryCrud.get();
    ctx.body = {
      success: 1,
      data: {
        country
      },
      message: 'country fetched successfully'
    };
  } catch (e) {
    ctx.throw(404, {
      success: 0,
      message: e.message
    });
  }
};

/**
@api {get} /api/state Get all states
@apiName Get all states
@apiGroup State
@apiSuccessExample {json} Success
{
  success: 1,
  data: {
    state: [Object]
  },
  message: state fetched successfully
}
@apiErrorExample {json} Server error
  HTTP/1.1 500 Internal Server Error
 */

export const stateGet = async (ctx) => {
  try {
    state = await stateCrud.get();
    ctx.body = {
      success: 1,
      data: {
        state
      },
      message: 'state fetched successfully'
    };
  } catch (e) {
    ctx.throw(404, {
      success: 0,
      message: e.message
    });
  }
};

/**
@api {get} /api/city Get all cities
@apiName Get all cities
@apiGroup City
@apiSuccessExample {json} Success
{
  success: 1,
  data: {
    city: [Object]
  },
  message: city fetched successfully
}
@apiErrorExample {json} Server error
  HTTP/1.1 500 Internal Server Error
 */

export const cityGet = async (ctx) => {
  try {
    city = await cityCrud.get();
    ctx.body = {
      success: 1,
      data: {
        city
      },
      message: 'city fetched successfully'
    };
  } catch (e) {
    ctx.throw(404, {
      success: 0,
      message: e.message
    });
  }
};

/**
@api {get} /api/country/:id Get single country
@apiName Get single country
@apiGroup Country
@apiSuccessExample {json} Success
{
  success: 1,
  data: {
    country: [Object]
  },
  message: country details fetched successfully
}
@apiErrorExample {json} Server error
  HTTP/1.1 500 Internal Server Error
 */

export const countryGetSingle = async (ctx) => {
  try {
    country = await countryCrud.single({
      qr: {
        _id: ctx.params.id
      }
    });
    ctx.body = {
      success: 1,
      data: {
        country
      },
      message: 'Country fetched successfully'
    };
  } catch (e) {
    ctx.throw(404, {
      success: 0,
      message: e.message
    });
  }
};

/**
@api {get} /api/state/:id Get single state
@apiName Get single state
@apiGroup State
@apiSuccessExample {json} Success
{
  success: 1,
  data: {
    state: [Object]
  },
  message: state details fetched successfully
}
@apiErrorExample {json} Server error
  HTTP/1.1 500 Internal Server Error
 */

export const stateGetSingle = async (ctx) => {
  try {
    state = await stateCrud.single({
      qr: {
        _id: ctx.params.id
      }
    });
    ctx.body = {
      success: 1,
      data: {
        state
      },
      message: 'state fetched successfully'
    };
  } catch (e) {
    ctx.throw(404, {
      success: 0,
      message: e.message
    });
  }
};

/**
@api {get} /api/city/:id Get single city
@apiName Get single city
@apiGroup City
@apiSuccessExample {json} Success
{
  success: 1,
  data: {
    city: [Object]
  },
  message: city details fetched successfully
}
@apiErrorExample {json} Server error
  HTTP/1.1 500 Internal Server Error
 */

export const cityGetSingle = async (ctx) => {
  try {
    city = await cityCrud.single({
      qr: {
        _id: ctx.params.id
      }
    });
    ctx.body = {
      success: 1,
      data: {
        city
      },
      message: 'city fetched successfully'
    };
  } catch (e) {
    ctx.throw(404, {
      success: 0,
      message: e.message
    });
  }
};

/**
@api {get} /api/state/:id/country Get all states in the country
@apiName Get all states in the country
@apiGroup State
@apiSuccessExample {json} Success
{
  success: 1,
  data: {
    state: [Object]
  },
  message: state details fetched successfully
}
@apiErrorExample {json} Server error
  HTTP/1.1 500 Internal Server Error
 */

export const getStateByCountry = async (ctx) => {
  try {
    state = await stateCrud.get({
      qr: {
        countryId: ctx.params.id
      }
    });
    ctx.body = {
      success: 1,
      data: {
        state
      },
      message: 'state fetched successfully'
    };
  } catch (e) {
    ctx.throw(404, {
      success: 0,
      message: e.message
    });
  }
};

/**
@api {get} /api/city/:id/state Get all cities in the state
@apiName Get all cities in the state
@apiGroup City
@apiSuccessExample {json} Success
{
  success: 1,
  data: {
    city: [Object]
  },
  message: city details fetched successfully
}
@apiErrorExample {json} Server error
  HTTP/1.1 500 Internal Server Error
 */

export const getCityByState = async (ctx) => {
  try {
    city = await cityCrud.get({
      qr: {
        stateId: ctx.params.id
      }
    });
    ctx.body = {
      success: 1,
      data: {
        city
      },
      message: 'city fetched successfully'
    };
  } catch (e) {
    ctx.throw(404, {
      success: 0,
      message: e.message
    });
  }
};

/**
@api {post} /api/country/:id Delete country
@apiName Delete country
@apiGroup Country
@apiSuccessExample {json} Success
{
  success: 1,
  data: {
    country: [Object]
  },
  message: country deleted successfully
}
@apiErrorExample {json} Server error
  HTTP/1.1 500 Internal Server Error
*/


export const deleteCountry = async (ctx) => {
  try {
    country = await countryCrud.delete({
      params: {
        qr: {
          _id: ctx.params.id
        }
      }
    });
    ctx.body = {
      success: 1,
      data: {
        country
      },
      message: 'Country deleted successfully'
    };
  } catch (e) {
    ctx.throw(404, {
      success: 0,
      message: e.message
    });
  }
};

/**
@api {post} /api/state/:id Delete state
@apiName Delete state
@apiGroup State
@apiSuccessExample {json} Success
{
  success: 1,
  data: {
    state: [Object]
  },
  message: state deleted successfully
}
@apiErrorExample {json} Server error
  HTTP/1.1 500 Internal Server Error
*/

export const deleteState = async (ctx) => {
  try {
    state = await stateCrud.delete({
      params: {
        qr: {
          _id: ctx.params.id
        }
      }
    });
    ctx.body = {
      success: 1,
      data: {
        state
      },
      message: 'state deleted successfully'
    };
  } catch (e) {
    ctx.throw(404, {
      success: 0,
      message: e.message
    });
  }
};

/**
@api {post} /api/city/:id Delete city
@apiName Delete city
@apiGroup City
@apiSuccessExample {json} Success
{
  success: 1,
  data: {
    city: [Object]
  },
  message: city deleted successfully
}
@apiErrorExample {json} Server error
  HTTP/1.1 500 Internal Server Error
*/

export const deleteCity = async (ctx) => {
  try {
    city = await cityCrud.delete({
      params: {
        qr: {
          _id: ctx.params.id
        }
      }
    });
    ctx.body = {
      success: 1,
      data: {
        city
      },
      message: 'city deleted successfully'
    };
  } catch (e) {
    ctx.throw(404, {
      success: 0,
      message: e.message
    });
  }
};

