import mongoose from 'mongoose';
import uniqueValidator from 'mongoose-unique-validator';
import timestamp from 'mongoose-timestamp';
import { Crud } from '@utl';

const countrySchema = new mongoose.Schema({
  countryName: String
});

countrySchema.plugin(uniqueValidator);
countrySchema.plugin(timestamp);

const countryModel = mongoose.model('countryModel', countrySchema);
const countryCrud = new Crud(countryModel);

export {
  countryModel,
  countryCrud
};
