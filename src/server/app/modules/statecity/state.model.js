import mongoose from 'mongoose';
import uniqueValidator from 'mongoose-unique-validator';
import timestamp from 'mongoose-timestamp';
import { Crud } from '@utl';

const stateSchema = new mongoose.Schema({
  stateName: String,
  countryId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'countryModel',
    default: null
  }
});

stateSchema.plugin(uniqueValidator);
stateSchema.plugin(timestamp);

const stateModel = mongoose.model('stateModel', stateSchema);
const stateCrud = new Crud(stateModel);

export {
  stateModel,
  stateCrud
};
