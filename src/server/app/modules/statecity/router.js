// import { isAuthenticated } from '@mid';

import {
  countryCreate,
  stateCreate,
  cityCreate,
  countryGet,
  stateGet,
  cityGet,
  countryGetSingle,
  stateGetSingle,
  cityGetSingle,
  deleteCountry,
  deleteState,
  deleteCity,
  getStateByCountry,
  getCityByState
} from './';

export const baseUrl = '/api';

export const routes = [
  {
    method: 'GET',
    route: '/country',
    handlers: [
      countryGet
    ]
  },
  {
    method: 'GET',
    route: '/state',
    handlers: [
      stateGet
    ]
  },
  {
    method: 'GET',
    route: '/city',
    handlers: [
      cityGet
    ]
  },
  {
    method: 'GET',
    route: '/country/:id',
    handlers: [
      countryGetSingle
    ]
  },
  {
    method: 'GET',
    route: '/state/:id',
    handlers: [
      stateGetSingle
    ]
  },
  {
    method: 'GET',
    route: '/state/:id/country',
    handlers: [
      getStateByCountry
    ]
  },
  {
    method: 'GET',
    route: '/city/:id',
    handlers: [
      cityGetSingle
    ]
  },
  {
    method: 'GET',
    route: '/city/:id/state',
    handlers: [
      getCityByState
    ]
  },
  {
    method: 'DELETE',
    route: '/country/:id',
    handlers: [
      deleteCountry
    ]
  },
  {
    method: 'DELETE',
    route: '/state/:id',
    handlers: [
      deleteState
    ]
  },
  {
    method: 'DELETE',
    route: '/city/:id',
    handlers: [
      deleteCity
    ]
  },
  {
    method: 'POST',
    route: '/country',
    handlers: [
      countryCreate
    ]
  },
  {
    method: 'POST',
    route: '/state',
    handlers: [
      stateCreate
    ]
  },
  {
    method: 'POST',
    route: '/city',
    handlers: [
      cityCreate
    ]
  }
];
