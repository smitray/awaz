import { generate } from 'shortid';
import { join } from 'path';
import sharp from 'sharp';
import fs from 'fs-extra';
import config from 'config';
import ffmpeg from 'fluent-ffmpeg';

import { filesCrud, filesModel } from './files.model';


let filesNew;

const fileUpload = async (file) => {
  let thumbFile;
  const fileType = file.type.split('/')[0];
  const ext = (file.name).split('.');
  const fileName = `${Date.now()}-${generate()}`;
  const newFilename = `${fileName}.${ext[1]}`;
  const filePath = join(config.get('paths.static'), newFilename);
  try {
    await fs.copy(file.path, filePath);
    if (fileType === 'image') {
      thumbFile = `${fileName}.png`;
      await sharp(filePath)
        .resize(200, 200)
        .png()
        .toFile(join(config.get('paths.static'), `thumbs/${thumbFile}`));
    } else if (fileType === 'application') {
      thumbFile = 'generic/default-file.png';
    } else if (fileType === 'video') {
      thumbFile = `${fileName}.png`;
      await ffmpeg(filePath)
        .screenshots({
          timestamps: ['5%'],
          filename: thumbFile,
          folder: join(config.get('paths.static'), 'thumbs/'),
          size: '320x240'
        });
    }
  } catch (e) {
    throw new Error(e.message);
  }
  return {
    file: newFilename,
    thumbnail: thumbFile
  };
};

const bulkUpdate = options => new Promise((resolve, reject) => {
  filesModel.insertMany(options).then((result) => {
    resolve(result);
  }).catch((err) => {
    reject(err);
  });
});


const filesCreate = async (ctx) => {
  const rawFiles = ctx.request.body.files.docs;
  const fileNames = [];
  let filename;
  if (rawFiles instanceof Array) {
    await Promise.all(rawFiles.map(async (file) => {
      filename = await fileUpload(file);
      fileNames.push({
        filename: filename.file,
        permalink: `/public/${filename.file}`,
        thumbnail: `/public/thumbs/${filename.thumbnail}`
      });
    }));

    try {
      filesNew = await bulkUpdate(fileNames);
    } catch (e) {
      ctx.throw(422, {
        success: 0,
        message: e.message
      });
    } finally {
      ctx.body = {
        success: 1,
        data: {
          files: filesNew,
          fields: {
            ...ctx.request.body.fields
          }
        },
        message: 'All files uploaded'
      };
    }
  } else {
    filename = await fileUpload(rawFiles);
    try {
      filesNew = await filesCrud.create({
        filename: filename.file,
        permalink: `/public/${filename.file}`,
        thumbnail: `/public/thumbs/${filename.thumbnail}`
      });
    } catch (e) {
      ctx.throw(422, e.message);
    } finally {
      ctx.body = {
        success: 1,
        data: {
          files: filesNew,
          fields: {
            ...ctx.request.body.fields
          }
        },
        message: 'File uploaded'
      };
    }
  }
};

export default filesCreate;
