import _ from 'lodash';

import { chatCrud } from './';
import { filesCrud } from '../files';


let chat;

export const getAllChat = async (ctx) => {
  try {
    chat = await chatCrud.get({
      populate: [{
        path: 'groupImage',
        model: 'filesModel'
      }]
    });
    ctx.body = {
      success: 1,
      data: {
        chat
      },
      message: 'group updated successfully'
    };
  } catch (error) {
    ctx.throw(422, {
      success: 0,
      message: error.message
    });
  }
};


export const chatUpdate = async (ctx) => {
  try {
    const body = _.pick(ctx.request.body, ['group', 'groupImage']);
    chat = await chatCrud.put({
      params: {
        qr: {
          _id: ctx.params.id
        }
      },
      body
    });
    let grImg = null;
    if (body.groupImage) {
      grImg = await filesCrud.single({
        qr: {
          _id: body.groupImage
        }
      });
    }
    chat.users.forEach((user) => {
      ctx.app.io.to(user).emit('group-update', {
        group: body.group,
        groupImage: grImg,
        chatId: ctx.params.id
      });
    });
    ctx.body = {
      success: 1,
      message: 'group updated successfully'
    };
  } catch (error) {
    ctx.throw(422, {
      success: 0,
      message: error.message
    });
  }
};

export const chatInfo = async (ctx) => {
  try {
    chat = await chatCrud.single({
      qr: {
        _id: ctx.params.id
      },
      populate: [{
        path: 'users',
        model: 'userModel'
      }, {
        path: 'stream',
        model: 'streamModel'
      }, {
        path: 'groupImage',
        model: 'filesModel'
      }]
    });
    ctx.body = {
      success: 1,
      data: {
        chat
      },
      message: 'chat fetched successfully'
    };
  } catch (e) {
    ctx.throw(422, {
      success: 0,
      message: e.message
    });
  }
};
