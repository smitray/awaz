import { isAuthenticated } from '@mid';

import {
  chatUpdate,
  chatInfo,
  getAllChat
} from './';

export const baseUrl = '/api/chat';

export const routes = [
  {
    method: 'GET',
    route: '/',
    handlers: [
      getAllChat
    ]
  },
  {
    method: 'PUT',
    route: '/:id',
    handlers: [
      isAuthenticated,
      chatUpdate
    ]
  },
  {
    method: 'GET',
    route: '/:id',
    handlers: [
      isAuthenticated,
      chatInfo
    ]
  }
];
