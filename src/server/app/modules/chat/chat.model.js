import mongoose from 'mongoose';
import uniqueValidator from 'mongoose-unique-validator';
import timestamp from 'mongoose-timestamp';

const chatSchema = new mongoose.Schema({
  users: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'userModel',
    default: null
  }],
  status: {
    type: String,
    default: null
  },
  stream: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'streamModel',
    default: null
  }],
  chat_type: {
    type: String,
    default: 'chat'
  },
  group: {
    type: String,
    default: null
  },
  groupImage: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'filesModel',
    default: null
  },
  admin: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'userModel',
    default: null
  }]
});

chatSchema.plugin(uniqueValidator);
chatSchema.plugin(timestamp);

const chatModel = mongoose.model('chatModel', chatSchema);

export default chatModel;
