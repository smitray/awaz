import { Crud } from '@utl';

import chatModel from './chat.model';
import { streamCrud } from './stream.model';

class Servicechat extends Crud {
  constructor(model) {
    super(model);
    this.model = model;
    this.streamCrud = streamCrud;
  }

  async createChat(options) {
    const {
      stType,
      content,
      file,
      from,
      to,
      postTime,
      chatType,
      group,
      groupImage
    } = options;
    let streamData;

    if (chatType && chatType === 'group') {
      streamData = {
        st_type: stType,
        content,
        file,
        from,
        post_time: postTime
      };
    } else {
      streamData = {
        st_type: stType,
        content,
        file,
        from,
        to,
        post_time: postTime
      };
    }
    const stream = await this.streamCrud.create(streamData);
    const chat = await this.create({
      chat_type: chatType,
      group,
      groupImage
    });
    chat.users.push(options.from);
    if (chatType && chatType === 'group') {
      options.to.forEach((user) => {
        chat.users.push(user);
      });
    } else {
      chat.users.push(options.to);
    }
    chat.stream.push(stream._id);

    if (chatType && chatType === 'group') {
      chat.admin.push(options.from);
    }

    return new Promise((resolve, reject) => {
      chat.save().then((result) => {
        resolve(result);
      }).catch((e) => {
        reject(e);
      });
    });
  }
}

const chatCrud = new Servicechat(chatModel);
export default chatCrud;
