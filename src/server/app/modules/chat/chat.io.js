import _ from 'lodash';

import { chatCrud, streamCrud } from './';
import { userCrud } from '../user';
import { filesCrud } from '../files';

// function getPaginatedItems(items, page, pageSize) {
//   const pg = page || 1;
//   const pgSize = pageSize || 20;
//   const offset = (pg - 1) * pgSize;
//   const pagedItems = _.drop(items, offset).slice(0, pgSize);
//   return {
//     page: pg,
//     pageSize: pgSize,
//     total: items.length,
//     total_pages: Math.ceil(items.length / pgSize),
//     data: pagedItems
//   };
// }

async function getUser(uid) {
  let user;

  try {
    user = await userCrud.single({
      qr: {
        _id: uid
      },
      select: 'full_name gender occupation dob email city state country blood_group donor phone dp last_seen',
      populate: [{
        path: 'dp',
        model: 'filesModel'
      }]
    });
    return user;
  } catch (e) {
    throw new Error(e.message);
  }
}

function chatStream(socket, app) {
  socket.on('message-received', async ({ streamId, from, chatId }) => {
    try {
      await streamCrud.put({
        params: {
          qr: {
            _id: streamId
          }
        },
        body: {
          received: true
        }
      });
      app.io.to(from).emit('message-received', {
        streamId,
        chatId
      });
    } catch (e) {
      throw new Error(e.message);
    }
  });

  socket.on('message-read', async ({ streamId, from, chatId }) => {
    try {
      await streamCrud.put({
        params: {
          qr: {
            _id: streamId
          }
        },
        body: {
          read: true
        }
      });
      app.io.to(from).emit('message-read', {
        streamId,
        chatId
      });
    } catch (e) {
      throw new Error(e.message);
    }
  });

  socket.on('message-fetch', async ({
    chatId,
    // page,
    // size,
    from
  }) => {
    try {
      const chats = await chatCrud.single({
        qr: {
          _id: chatId
        },
        populate: [{
          path: 'stream',
          model: 'streamModel',
          match: {
            read: false
          },
          populate: [{
            path: 'from',
            model: 'userModel'
          }, {
            path: 'to',
            model: 'userModel'
          }, {
            path: 'file',
            model: 'filesModel'
          }]
        }]
      });
      // const streamIds = getPaginatedItems(chat.stream, page, size);
      // const chats = [];
      // streamIds.data.forEach(async (item) => {
      //   const stream = await streamCrud.single({
      //     qr: {
      //       _id: item,
      //       from
      //     }
      //   });
      //   const {
      //     content,
      //     file,
      //     frm,
      //     to
      //   } = stream;
      //   chats.push({
      //     stType: stream.st_type,
      //     content,
      //     file,
      //     from: frm,
      //     to,
      //     postTime: stream.post_time
      //   });
      // });
      app.io.to(from).emit('message-fetch', {
        chatId,
        // page,
        // size,
        chats
      });
    } catch (e) {
      throw new Error(e.message);
    }
  });

  socket.on('message-delete', async ({
    streamId,
    from,
    to,
    chatId
  }) => {
    try {
      await streamCrud.delete({
        params: {
          qr: {
            _id: streamId
          }
        }
      });
      app.io.to(from).emit('message-delete', {
        streamId,
        chatId
      });
      app.io.to(to).emit('message-delete', {
        streamId,
        chatId
      });
    } catch (e) {
      throw new Error(e.message);
    }
  });

  socket.on('add-user', async (data) => {
    try {
      const {
        chatId,
        uid,
        from
      } = data;
      const chat = await chatCrud.single({
        qr: {
          _id: chatId
        }
      });
      chat.users.push(uid);
      await chat.save();
      const user = await getUser(uid);
      const fromUser = await getUser(from);
      chat.users.forEach(async (toUser) => {
        app.io.to(toUser).emit('add-user', {
          chatId,
          user,
          fromUser
        });
      });
    } catch (e) {
      throw new Error(e.message);
    }
  });

  socket.on('remove-user', async (data) => {
    try {
      const {
        chatId,
        uid,
        from
      } = data;
      const chat = await chatCrud.single({
        qr: {
          _id: chatId
        }
      });
      _.remove(chat.users, e => e._id === uid);
      await chat.save();
      const user = await getUser(uid);
      const fromUser = await getUser(from);
      chat.users.forEach(async (toUser) => {
        app.io.to(toUser).emit('remove-user', {
          chatId,
          user,
          fromUser
        });
      });
    } catch (e) {
      throw new Error(e.message);
    }
  });
  socket.on('add-admin', async (data) => {
    try {
      const {
        chatId,
        uid,
        from
      } = data;
      const chat = await chatCrud.single({
        qr: {
          _id: chatId
        }
      });
      chat.admin.push(uid);
      await chat.save();
      const user = await getUser(uid);
      const fromUser = await getUser(from);
      chat.users.forEach(async (toUser) => {
        app.io.to(toUser).emit('add-admin', {
          chatId,
          user,
          fromUser
        });
      });
    } catch (e) {
      throw new Error(e.message);
    }
  });

  socket.on('remove-admin', async (data) => {
    try {
      const {
        chatId,
        uid,
        from
      } = data;
      const chat = await chatCrud.single({
        qr: {
          _id: chatId
        }
      });
      _.remove(chat.admin, e => e._id === uid);
      await chat.save();
      const user = await getUser(uid);
      const fromUser = await getUser(from);
      chat.users.forEach(async (toUser) => {
        app.io.to(toUser).emit('remove-admin', {
          chatId,
          user,
          fromUser
        });
      });
    } catch (e) {
      throw new Error(e.message);
    }
  });

  socket.on('change-name', async (data) => {
    try {
      const {
        chatId,
        group
      } = data;
      const chat = await chatCrud.single({
        qr: {
          _id: chatId
        }
      });
      chat.group = group;
      await chat.save();
      chat.users.forEach(async (toUser) => {
        app.io.to(toUser).emit('change-name', {
          chatId,
          group
        });
      });
    } catch (e) {
      throw new Error(e.message);
    }
  });

  socket.on('chat-request', async (data) => {
    try {
      const chatNew = await chatCrud.createChat({
        ...data
      });
      const {
        stType,
        content,
        file,
        from,
        to,
        postTime,
        chatType,
        group,
        tempId,
        fileMeta
      } = data;
      let fileDet = null;
      if (stType === 'file') {
        fileDet = await filesCrud.single({
          qr: {
            _id: file
          }
        });
      }
      const fromUser = await getUser(data.from);
      if (data.chatType && data.chatType === 'group') {
        const groupUsers = [];
        let grImg = null;
        if (data.groupImage) {
          grImg = await filesCrud.single({
            qr: {
              _id: data.groupImage
            }
          });
        }
        data.to.forEach(async (toUser) => {
          groupUsers.push(await getUser(toUser));
          app.io.to(toUser).emit('chat-request', {
            stType,
            content,
            file: fileDet,
            from,
            to,
            postTime,
            chatType,
            group,
            fileMeta,
            tempId,
            chatId: chatNew._id,
            streamId: chatNew.stream[0],
            fromUser,
            groupUsers,
            groupImage: grImg
          });
        });
        app.io.to(data.from).emit('chat-request', {
          stType,
          content,
          file: fileDet,
          from,
          to,
          postTime,
          chatType,
          group,
          fileMeta,
          tempId,
          chatId: chatNew._id,
          streamId: chatNew.stream[0],
          sent: true,
          fromUser,
          groupUsers,
          groupImage: grImg
        });
      } else {
        const toUser = await getUser(data.to);
        app.io.to(data.to).emit('chat-request', {
          fromUser,
          toUser,
          stType,
          content,
          file: fileDet,
          from,
          to,
          postTime,
          chatType,
          group,
          fileMeta,
          tempId,
          chatId: chatNew._id,
          streamId: chatNew.stream[0]
        });
        app.io.to(data.from).emit('chat-request', {
          fromUser,
          toUser,
          stType,
          content,
          file: fileDet,
          from,
          to,
          postTime,
          chatType,
          group,
          fileMeta,
          tempId,
          chatId: chatNew._id,
          streamId: chatNew.stream[0],
          sent: true
        });
      }
    } catch (e) {
      throw new Error(e.message);
    }
  });

  socket.on('room-message', async ({
    chatId,
    from,
    to,
    stType,
    content,
    file,
    postTime,
    tempId,
    toPhone,
    fromPhone,
    chatType,
    fileMeta
  }) => {
    try {
      let streamData;
      let fileDet = null;
      if (stType === 'file') {
        fileDet = await filesCrud.single({
          qr: {
            _id: file
          }
        });
      }
      if (chatType && chatType === 'group') {
        streamData = {
          st_type: stType,
          content,
          file,
          from,
          post_time: postTime
        };
      } else {
        streamData = {
          st_type: stType,
          content,
          file,
          from,
          to,
          post_time: postTime
        };
      }
      const stream = await streamCrud.create(streamData);
      const chat = await chatCrud.single({
        qr: {
          _id: chatId
        }
      });
      chat.stream.push(stream._id);
      await chat.save();
      if (chatType && chatType === 'group') {
        _.remove(chat.users, e => e._id === from);
        chat.users.forEach(async (toUser) => {
          app.io.to(toUser).emit('room-message', {
            chatId,
            from,
            to,
            stType,
            content,
            file: fileDet,
            postTime,
            streamId: stream._id,
            tempId,
            toPhone,
            fromPhone,
            fileMeta,
            chatType
          });
        });
      } else {
        app.io.to(to).emit('room-message', {
          chatId,
          from,
          to,
          stType,
          content,
          file: fileDet,
          postTime,
          streamId: stream._id,
          tempId,
          toPhone,
          fromPhone,
          fileMeta
        });
      }
      app.io.to(from).emit('room-message', {
        chatId,
        from,
        to,
        stType,
        content,
        file: fileDet,
        postTime,
        sent: true,
        streamId: stream._id,
        tempId,
        toPhone,
        fromPhone,
        fileMeta,
        chatType
      });
    } catch (e) {
      throw new Error(e.message);
    }
  });
}

export default chatStream;
