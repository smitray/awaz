// import { isAuthenticated } from '@mid';

import { groupController } from './';

const {
  groupAll,
  groupSingle,
  groupCreate,
  groupUpdate,
  groupDelete
} = groupController;

export const baseUrl = '/api/group';

export const routes = [
  {
    method: 'GET',
    route: '/',
    handlers: [
      groupAll
    ]
  },
  {
    method: 'GET',
    route: '/:id',
    handlers: [
      groupSingle
    ]
  },
  {
    method: 'PUT',
    route: '/:id',
    handlers: [
      groupUpdate
    ]
  },
  {
    method: 'DELETE',
    route: '/:id',
    handlers: [
      groupDelete
    ]
  },
  {
    method: 'POST',
    route: '/',
    handlers: [
      groupCreate
    ]
  }
];
