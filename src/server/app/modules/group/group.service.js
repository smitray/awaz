import { Crud } from '@utl';

import groupModel from './group.model';

class Servicegroup extends Crud {
  constructor(model) {
    super(model);
    this.model = model;
  }
}

const groupCrud = new Servicegroup(groupModel);
export default groupCrud;
