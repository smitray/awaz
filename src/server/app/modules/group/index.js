export * as groupRouteProps from './router';
export * as groupController from './controller';
export { default as groupModel } from './group.model';
export { default as groupCrud } from './group.service';

