import { groupCrud } from './';


let groups;
let group;
let groupNew;

const groupAll = async (ctx) => {
  try {
    groups = await groupCrud.get();
  } catch (e) {
    ctx.throw(404, {
      success: 0,
      message: e.message
    });
  } finally {
    ctx.body = {
      success: 1,
      data: {
        groups
      },
      message: 'groups fetched successfully'
    };
  }
};

const groupSingle = async (ctx) => {
  try {
    group = await groupCrud.single({
      qr: {
        _id: ctx.params.id
      }
    });
  } catch (e) {
    ctx.throw(404, {
      success: 0,
      message: e.message
    });
  } finally {
    ctx.body = {
      success: 1,
      data: {
        group
      },
      message: 'group fetched successfully'
    };
  }
};

const groupCreate = async (ctx) => {
  try {
    groupNew = await groupCrud.create(ctx.request.body);
  } catch (e) {
    ctx.throw(422, {
      success: 0,
      message: e.message
    });
  } finally {
    ctx.body = {
      success: 1,
      data: {
        group: groupNew
      },
      message: 'group created successfully'
    };
  }
};

const groupUpdate = async (ctx) => {
  try {
    group = await groupCrud.put({
      params: {
        qr: {
          _id: ctx.params.id
        }
      },
      body: ctx.request.body
    });
  } catch (e) {
    ctx.throw(422, {
      success: 0,
      message: e.message
    });
  } finally {
    ctx.body = {
      success: 1,
      data: {
        group
      },
      message: 'group updated successfully'
    };
  }
};

const groupDelete = async (ctx) => {
  try {
    group = await groupCrud.delete({
      params: {
        qr: {
          _id: ctx.params.id
        }
      }
    });
  } catch (e) {
    ctx.throw(404, {
      success: 0,
      message: e.message
    });
  } finally {
    ctx.body = {
      success: 1,
      data: {
        group
      },
      message: 'group deleted successfully'
    };
  }
};

export {
  groupAll,
  groupSingle,
  groupCreate,
  groupUpdate,
  groupDelete
};
