import Router from 'koa-router';

import { userRouteProps, userIO } from './user';
import { chatIO, chatRouteProps } from './chat';
import { filesRouteProps } from './files';
import { statecityRouteProps } from './statecity';

const routerControllPros = [
  userRouteProps,
  filesRouteProps,
  chatRouteProps,
  statecityRouteProps
];

const ioControlProps = [
  userIO,
  chatIO
];

const ioControl = (app) => {
  app.io.on('connection', (socket) => {
    ioControlProps.forEach((ioProperty) => {
      ioProperty(socket, app);
    });

    socket.on('disconnect', () => {
      console.log('A user disconnected');
    });
  });
};


let instance;

const routerControl = (app) => {
  routerControllPros.forEach((routeProperty) => {
    instance = new Router({ prefix: routeProperty.baseUrl });
    routeProperty.routes.forEach((config) => {
      const {
        method = '',
        route = '',
        handlers = []
      } = config;

      const lastHandler = handlers.pop();

      instance[method.toLowerCase()](route, ...handlers, async (ctx) => {
        const hddd = await lastHandler(ctx);
        return hddd;
      });

      app
        .use(instance.routes())
        .use(instance.allowedMethods());
    });
  });
};

export {
  routerControl,
  ioControl
};
