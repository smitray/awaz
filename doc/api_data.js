define({ "api": [
  {
    "type": "post",
    "url": "/api/city",
    "title": "Create city",
    "name": "Create_city",
    "group": "City",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "cityName",
            "description": "<p>Name of the city</p>"
          },
          {
            "group": "Parameter",
            "type": "ObjectId",
            "optional": false,
            "field": "stateId",
            "description": "<p>ID of the state</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Input",
          "content": "{\n  cityName: \"Bangalore\",\n  stateId: [ObjectId]\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n  success: 1,\n  data: {\n    city: [Object]\n  },\n  message: city created successfully\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Server error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/server/app/modules/statecity/controller.js",
    "groupTitle": "City"
  },
  {
    "type": "post",
    "url": "/api/city/:id",
    "title": "Delete city",
    "name": "Delete_city",
    "group": "City",
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n  success: 1,\n  data: {\n    city: [Object]\n  },\n  message: city deleted successfully\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Server error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/server/app/modules/statecity/controller.js",
    "groupTitle": "City"
  },
  {
    "type": "get",
    "url": "/api/city",
    "title": "Get all cities",
    "name": "Get_all_cities",
    "group": "City",
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n  success: 1,\n  data: {\n    city: [Object]\n  },\n  message: city fetched successfully\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Server error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/server/app/modules/statecity/controller.js",
    "groupTitle": "City"
  },
  {
    "type": "get",
    "url": "/api/city/:id/state",
    "title": "Get all cities in the state",
    "name": "Get_all_cities_in_the_state",
    "group": "City",
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n  success: 1,\n  data: {\n    city: [Object]\n  },\n  message: city details fetched successfully\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Server error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/server/app/modules/statecity/controller.js",
    "groupTitle": "City"
  },
  {
    "type": "get",
    "url": "/api/city/:id",
    "title": "Get single city",
    "name": "Get_single_city",
    "group": "City",
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n  success: 1,\n  data: {\n    city: [Object]\n  },\n  message: city details fetched successfully\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Server error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/server/app/modules/statecity/controller.js",
    "groupTitle": "City"
  },
  {
    "type": "post",
    "url": "/api/country",
    "title": "Create country",
    "name": "Create_country",
    "group": "Country",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "countryName",
            "description": "<p>Name of the country</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Input",
          "content": "{\n  countryName: \"India\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n  success: 1,\n  data: {\n    country: [Object]\n  },\n  message: country created successfully\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Server error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/server/app/modules/statecity/controller.js",
    "groupTitle": "Country"
  },
  {
    "type": "post",
    "url": "/api/country/:id",
    "title": "Delete country",
    "name": "Delete_country",
    "group": "Country",
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n  success: 1,\n  data: {\n    country: [Object]\n  },\n  message: country deleted successfully\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Server error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/server/app/modules/statecity/controller.js",
    "groupTitle": "Country"
  },
  {
    "type": "get",
    "url": "/api/country",
    "title": "Get all countries",
    "name": "Get_all_countries",
    "group": "Country",
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n  success: 1,\n  data: {\n    country: [Object]\n  },\n  message: country fetched successfully\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Server error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/server/app/modules/statecity/controller.js",
    "groupTitle": "Country"
  },
  {
    "type": "get",
    "url": "/api/country/:id",
    "title": "Get single country",
    "name": "Get_single_country",
    "group": "Country",
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n  success: 1,\n  data: {\n    country: [Object]\n  },\n  message: country details fetched successfully\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Server error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/server/app/modules/statecity/controller.js",
    "groupTitle": "Country"
  },
  {
    "type": "post",
    "url": "/api/state",
    "title": "Create state",
    "name": "Create_state",
    "group": "State",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "stateName",
            "description": "<p>Name of the state</p>"
          },
          {
            "group": "Parameter",
            "type": "ObjectId",
            "optional": false,
            "field": "countryId",
            "description": "<p>ID of the country</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Input",
          "content": "{\n  stateName: \"Karnataka\",\n  countryId: [ObjectId]\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n  success: 1,\n  data: {\n    state: [Object]\n  },\n  message: state created successfully\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Server error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/server/app/modules/statecity/controller.js",
    "groupTitle": "State"
  },
  {
    "type": "post",
    "url": "/api/state/:id",
    "title": "Delete state",
    "name": "Delete_state",
    "group": "State",
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n  success: 1,\n  data: {\n    state: [Object]\n  },\n  message: state deleted successfully\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Server error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/server/app/modules/statecity/controller.js",
    "groupTitle": "State"
  },
  {
    "type": "get",
    "url": "/api/state",
    "title": "Get all states",
    "name": "Get_all_states",
    "group": "State",
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n  success: 1,\n  data: {\n    state: [Object]\n  },\n  message: state fetched successfully\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Server error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/server/app/modules/statecity/controller.js",
    "groupTitle": "State"
  },
  {
    "type": "get",
    "url": "/api/state/:id/country",
    "title": "Get all states in the country",
    "name": "Get_all_states_in_the_country",
    "group": "State",
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n  success: 1,\n  data: {\n    state: [Object]\n  },\n  message: state details fetched successfully\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Server error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/server/app/modules/statecity/controller.js",
    "groupTitle": "State"
  },
  {
    "type": "get",
    "url": "/api/state/:id",
    "title": "Get single state",
    "name": "Get_single_state",
    "group": "State",
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n  success: 1,\n  data: {\n    state: [Object]\n  },\n  message: state details fetched successfully\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Server error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/server/app/modules/statecity/controller.js",
    "groupTitle": "State"
  }
] });
