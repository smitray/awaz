[![forthebadge](https://forthebadge.com/images/badges/built-with-love.svg)](https://forthebadge.com)
[![forthebadge](https://forthebadge.com/images/badges/built-by-developers.svg)](https://forthebadge.com)
[![forthebadge](https://forthebadge.com/images/badges/built-for-android.svg)](https://forthebadge.com)
[![forthebadge](https://forthebadge.com/images/badges/made-with-javascript.svg)](https://forthebadge.com)

[![MIT Licence](https://badges.frapsoft.com/os/mit/mit.png?v=103)](https://opensource.org/licenses/mit-license.php)
[![dependencies Status](https://david-dm.org/boennemann/badges/status.svg)](https://david-dm.org/boennemann/badges)
[ ![Codeship Status for smitray/awaz](https://app.codeship.com/projects/697e5cb0-1db1-0136-2419-7ea3863b4121/status?branch=master)](https://app.codeship.com/projects/284768)


# Awaz

### The idea
this app is quite similar to whatsapp but intended to be built with more features.

### Technologies
We intended to implement the follwoing technologies and frameworks used to develop this app

* Nodejs (Koa framework) - all libraries can be found at package.json file
* XMPP protocol
* WebRTC - Peer.js
* ES6 - babel.js
* Testing framework
  * Jest
  * Supertest
* Sending request to SMS api : request module

### Sripts

```javascript

"scripts": {
  "dev": "nodemon bin/server.dev.js",
  "prebuild": "rimraf dist",
  "build": "node bin/server.build.js",
  "test": "node bin/app.test.js",
  "start": "cross-env NODE_ENV=production node dist/server/core"
},

```


### Documentation

[Gitlab pages](http://smitray.gitlab.io/awaz/)
[API Base Url](http://awaz.codends.net)
